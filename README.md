@Test
    void deleteEntitlement() {
        UserRepository myEntityRepository = mock(UserRepository.class);
        // Create an instance of the service with the mock repository
        UserInterfaceImpl myEntityService = new UserInterfaceImpl(myEntityRepository);
        // Define the entity ID to be deleted
        Long entityId = 1L;
        // Call the service method to delete the entity
        myEntityService.deleteEntitlement(entityId);
        // Verify that the deleteById method of the repository was called with the correct ID
        verify(myEntityRepository, times(1)).deleteById(entityId);
    }