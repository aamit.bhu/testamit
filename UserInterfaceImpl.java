package net.javaguides.springboot.service.Impl;

import net.javaguides.springboot.entity.User;
import net.javaguides.springboot.exception.ResourceNotFoundException;
import net.javaguides.springboot.repository.UserRepository;
import net.javaguides.springboot.service.UserInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;
@Service
public class UserInterfaceImpl implements UserInterface {
    @Autowired
    private UserRepository userRepository;
    @Override
    public User deleteUser(long userId) {
        User existingUser = this.userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found with id :" + userId));
        this.userRepository.delete(existingUser);
        return existingUser;
    }

    @Override
    public Optional<User> findById(long userId) {
        Optional<User> existingUser =this.userRepository.findById(userId);
        return existingUser;
    }
    public ResponseEntity<?> deleteEntitlement(Long id) {
        User existingUser = null;
        try {
            if (null != id ) {
                existingUser = this.userRepository.findById(id).orElse(null);
            }
            if (existingUser != null) {
                this.userRepository.deleteById(id);
                return ResponseEntity.ok().body("Entitlement deleted successfully");
            } else {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Entitlement not found");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("An error occurred while deleting the Entitlement");
        }
    }


}
